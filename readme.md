## [Verify questionnaire metadata](https://wiki.ucl.ac.uk/display/CTTEAM/Metadata+Pipeline)


### input.txt
- The archivist instance, for example: closer-archivist-alspac
- The prefix, for example: alspac\_04\_tf1pi.
- Note that this field can be blank, i.e. export PREFIX=


### output in csv format
|  Check                                                 | Output Table            |
| :----------------------------------------------------- | :---------------------- |
| questionItem labels start with qi_                     | qi\_label               |
| questionConstruct labels start with qc_                | qc\_label               |
| codeList labels start with cs_                         | cs\_label               |
| questionItems and questionConstruct labels match       | qi\_qc\_label           |
| questions have a response                              | qi\_response            |
| question has a literal                                 | qi\_literal             |
| Agency matches the CV                                  | agency                  |
| Study matches the agency                               | study\_agency           |
| no missing roman numerals on labels                    | missing\_labels         |
| labels do not contain space                            | labels\_w\_space        |
| condition labels match the first question in the logic | condition\_label\_logic |

- The .txt files in the main output directory lists failed checks for all instruments in the instance.
- There is a separate folder named prefix_dir which contains the output .txt for the prefix in the input.txt.
- A .txt file will be produced when a check has failed.
